import numpy as np
import pandas as pd
import matplotlib.pylab as plt

train_set = pd.read_csv("/home/edem/Desktop/Desktop/Tutorials/loan_prediction/train.csv",delimiter = ',',header = None)


test_set = pd.read_csv("/home/edem/Desktop/Desktop/Tutorials/loan_prediction/test.csv",delimiter = ',',header = None)

##Add columns names to the data
train_set.columns = ['Gender','Married','Dependents','Education','Self_Employed','ApplicantIncome','CoapplicantIncome','LoanAmount','Loan_Amount_Term','Credit_History','Property_Area','Loan_Status']
test_set.columns = ['Gender','Married','Dependents','Education','Self_Employed','ApplicantIncome','CoapplicantIncome','LoanAmount','Loan_Amount_Term','Credit_History','Property_Area']
Result = train_set["Loan_Status"]

##Calculate the mean,std and the percentiles of the data

train_set.describe()

#histogram of applicant income
train_set['ApplicantIncome'].hist(bins=50)
train_set['LoanAmount'].hist(bins=50)
#Boxplot for the distributions

train_set.boxplot(column='ApplicantIncome')
train_set.boxplot(column='ApplicantIncome', by = 'Education')# boxplot group by Education

## To check missing values in each columns
train_set.apply(lambda x: sum(x.isnull()),axis=0)

## Dealing with missing values....replacement by mean
train_set['LoanAmount'].fillna(train_set['LoanAmount'].mean(), inplace=True)
train_set['LoanAmount'].replace(0,train_set['LoanAmount'].mean(),inplace = True)
##Fill these NA with NO since most of the people are found in this category
train_set['Self_Employed'].fillna('No',inplace=True)
#Pivot tables
table = train_set.pivot_table(values='LoanAmount', index='Self_Employed' ,columns='Education', aggfunc=np.median)
# Define function to return value of this pivot_table
def fage(x):
     return table.loc[x['Self_Employed'],x['Education']]

# Replace missing values
#train_set['LoanAmount'].fillna(train_set[train_set['LoanAmount'].isnull()].apply(fage, axis=1), inplace=True)

###The outliers in ApplicantIncome, we use the log of the ApplicantIncome
train_set['LoanAmount_log'] = np.log(train_set['LoanAmount'])
train_set['LoanAmount_log'].hist(bins=20)
## It is possible that applicants have lower income but strong support co-applicants
train_set['TotalIncome'] = train_set['ApplicantIncome'] + train_set['CoapplicantIncome']
train_set['TotalIncome_log'] = np.log(train_set['TotalIncome'])

###replace -inf with 0 after the log transformation
train_set['LoanAmount_log'].replace("-inf",0, inplace = True)
##Plot the histogram of the log transform
train_set['LoanAmount_log'].hist(bins=20)

## Dataset is now ready for predictive modelling
#Sklearn requires all columns to be in numeric form. So we must transform all the categorical columns to numeric
from sklearn.preprocessing import LabelEncoder
var_mod = ['Gender','Married','Dependents','Education','Self_Employed','Property_Area','Loan_Status']
le = LabelEncoder()
for i in var_mod:
    train_set[i] = le.fit_transform(train_set[i])
#Importation of the needed modules
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import KFold   #For K-fold cross validation
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import metrics

# Function for making classification model and getting perfomance
def classification_model(model, data, predictors, target):
    #model fit
    model.fit(data[predictors],data[target])
    #Making prediction on train_set
    predictions = model.predict(data[predictors])
    #Print model accuracy
    accuracy = metrics.accuracy_score(predictions,data[target])
    print ("Accuracy : %s" % "{0:.3%}".format(accuracy))
    #Perform K-fold cross-validation with 5 folds
    kf = KFold(data.shape[0], n_folds=5)
    error = []
    for train,test in kf:
        #filtering training set
         train_predictors = (data[predictors].iloc[train,:])
         #Target used for training the algorithm
         train_target = data[target].iloc[train]
         #Train the algorithm using predictors and the target
         model.fit(train_predictors, train_target)
         #Record errors from each cross validation
         error.append(model.score(data[predictors].iloc[test,:], data[target].iloc[test]))
         print ("Cross-Validation Score : %s" % "{0:.3%}".format(np.mean(error)))
         #Making it possible to refered the model outside this function
         model.fit(data[predictors],data[target])

#logistics Regression Using credit history

target_var = 'Loan_Status'
model = LogisticRegression()
predictor_var = ['Credit_History']
classification_model(model, train_set,predictor_var,target_var)
#Combinatioin of variables for better accuracy
target_var = 'Loan_Status'
model = LogisticRegression()
predictor_var = ['Credit_History','Education','Married','Self_Employed','Property_Area']
classification_model(model, train_set,predictor_var,target_var)
##Decision Tree
target_var = 'Loan_Status'
model = DecisionTreeClassifier()
predictor_var = ['Credit_History','Loan_Amount_Term','LoanAmount_log']
classification_model(model, train_set,predictor_var,target_var)
#The accuracy increased rapidly with a drop in the cross validation . This is an overfitting issue
#Random forest
model = RandomForestClassifier(n_estimators=100)
predictor_var = ['Gender', 'Married', 'Dependents', 'Education',
       'Self_Employed', 'Loan_Amount_Term', 'Credit_History', 'Property_Area',
        'LoanAmount_log','TotalIncome_log']
classification_model(model, train_set,predictor_var,target_var)
#Accuracy of 100% with drop in cross validation is caused by overfitting
#we use only important faeutures only
important_feat = pd.Series(model.feature_importances_, index=predictor_var).sort_values(ascending=False)
print(important_feat)
#using the top 4 important variables
model = RandomForestClassifier(n_estimators=25,min_samples_split=25,max_depth =7,max_features = 1)
predictor_var = ['TotalIncome_log','LoanAmount_log','Credit_History','Dependents','Property_Area']
classification_model(model, train_set,predictor_var,target_var)

